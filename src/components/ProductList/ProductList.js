import React, { Component } from 'react';

class ProductList extends Component {
    render() {
        return (
            <div className="panel panel-info mt-1">
                <div className="panel-heading">
                    <h3 className="panel-title">Panel title</h3>
                </div>
                <div className="panel-body">

                    <table className="table table-hover">
                        <thead>
                            <tr>
                                <th>Stt</th>
                                <th>Mã</th>
                                <th>Tên</th>
                                <th>Giá</th>
                                <th>Trạng thái</th>
                                <th>Hàng động</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.props.children}
                        </tbody>
                    </table>

                </div>
            </div>
        );
    }
}

export default ProductList;